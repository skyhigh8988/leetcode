
public class ReverseBits {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}
	public int reverseBits(int n) {
		
		StringBuilder bny = new StringBuilder();
		String revstr= new String();
		Integer temp= new Integer(n);
		double sum=0;
		int len= Integer.toBinaryString(temp).length();//tranfer integer to sting
		
		for(int i=0;i<32-len;i++) //reverse and add zero up to 32bits
			bny.append(0);
		revstr=bny.append(Integer.toBinaryString(temp)).reverse().toString();
		
		for( int i=31; i>=0 ;i--) {
			if(revstr.charAt(i)=='1')
				sum=sum+Math.pow(2, 31-i);
		}//calculate the integer result
		
		return (int)Math.round(sum);
	}
}
