import java.util.*;


public class leetcode {

	public class ListNode {
	    int val;
	    ListNode next;
	    ListNode(int x) { val = x; }
	}


    public ListNode reverseList(ListNode head) {
        
    	ListNode prev= null;
    	ListNode curr;

    	while(head!=null){
    		curr=head.next;
    		head.next=prev;
    		prev=head;
    		head=curr;
    	}
    	return prev;

    }

/* We need to take three node into account at once,

the prev will represent as the null pointer to fullfill the while loop

*/

}

